import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobx/mobx.dart';
import 'package:event_bus/event_bus.dart';

EventBus eventBus = EventBus();
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class Counter {
  @observable
  int value = 0;

  final val = Observable(0);

  @action
  void increment() {
    value++;
  }
}

class AddMovie extends StatefulWidget {
  final Movie movie;
  AddMovie({Key? key, required this.movie}) : super(key: key);

  @override
  _AddMovieState createState() => _AddMovieState();
}

class _AddMovieState extends State<AddMovie> {
  Movie _movie = Movie(genres: []);
  TextEditingController _controllerTitle = TextEditingController();
  TextEditingController _controllerDirector = TextEditingController();
  TextEditingController _controllerSummary = TextEditingController();
  @override
  void initState() {
    _movie = widget.movie;
    _controllerTitle.text = _movie.title;
    _controllerDirector.text = _movie.director;
    _controllerSummary.text = _movie.summary;
    super.initState();
  }

  void updateMovie() {
    String _messages = "";
    if (_controllerTitle.text.isEmpty) {
      _messages = _messages + " | Please add title | ";
    }
    if (_controllerDirector.text.isEmpty) {
      _messages = _messages + " | Please add director | ";
    }
    if (_movie.genres.where((g) => g.choosed == true).toList().length == 0) {
      _messages = _messages + " | Please add genre | ";
    }
    if (_controllerSummary.text.isEmpty) {
      _messages = _messages + " | Please add summary | ";
    }
    if (_messages != "") {
      final snackBar = SnackBar(content: Text(_messages));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } else {
      eventBus.fire(DeleteMovie(id: widget.movie.id));
      eventBus.fire(Movie(
          title: _controllerTitle.text,
          director: _controllerDirector.text,
          summary: _controllerSummary.text,
          genres: _movie.genres));
      eventBus.fire(Refresh());
      Navigator.pop(context);
    }
  }

  void deleteMovie() {
    eventBus.fire(DeleteMovie(id: widget.movie.id));
    eventBus.fire(Refresh());
    Navigator.pop(context);
  }

  void setMovie() {
    String _messages = "";
    if (_controllerTitle.text.isEmpty) {
      _messages = _messages + " | Please add title | ";
    }
    if (_controllerDirector.text.isEmpty) {
      _messages = _messages + " | Please add director | ";
    }
    if (_movie.genres.where((g) => g.choosed == true).toList().length == 0) {
      _messages = _messages + " | Please add genre | ";
    }
    if (_controllerSummary.text.isEmpty) {
      _messages = _messages + " | Please add summary | ";
    }
    if (_messages != "") {
      final snackBar = SnackBar(content: Text(_messages));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } else {
      eventBus.fire(Movie(
          title: _controllerTitle.text,
          director: _controllerDirector.text,
          summary: _controllerSummary.text,
          genres: _movie.genres));
      eventBus.fire(Refresh());
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    var _scale = MediaQuery.of(context).size.width / 360;
    return Scaffold(
      appBar: AppBar(
        title:
            widget.movie.title == "" ? Text("New Movie") : Text("Edit Movie"),
      ),
      body: Column(
        children: [
          Container(height: 10),
          TextField(
            maxLength: 150,
            maxLines: 2,
            minLines: 1,
            controller: _controllerTitle,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              hintText: "Ex : Transformer",
              labelText: "Title",
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(),
              ),
            ),
          ),
          TextField(
            maxLength: 150,
            maxLines: 2,
            minLines: 1,
            controller: _controllerDirector,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              hintText: "Ex : Michael Bay",
              labelText: "Director",
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(
                16 * _scale, 5 * _scale, 16 * _scale, 5 * _scale),
            height: 20 * _scale,
            width: 328 * _scale,
            child: Text("Choose the Genre"),
          ),
          Wrap(
              children: _movie.genres.map((genre) {
            return InkWell(
              onTap: () {
                setState(() {
                  _movie
                      .genres[_movie.genres
                          .indexWhere((element) => element.name == genre.name)]
                      .choosed = !genre.choosed;
                });
              },
              child: Material(
                child: Container(
                  margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  padding: EdgeInsets.fromLTRB(10, 4, 10, 4),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                        width: 1,
                        color: genre.choosed ? Colors.blue : Colors.black),
                  ),
                  child: Text(
                    genre.name,
                    style: TextStyle(
                      color: genre.choosed ? Colors.blue : Colors.black,
                    ),
                  ),
                ),
              ),
            );
          }).toList()),
          TextField(
            maxLength: 250,
            maxLines: 3,
            minLines: 1,
            controller: _controllerSummary,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              hintText: "Ex : Summary",
              labelText: "Summary",
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(),
              ),
            ),
          ),
          widget.movie.title == ""
              ? ElevatedButton(onPressed: setMovie, child: Text("Add Movie"))
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                        onPressed: updateMovie, child: Text("Edit Movie")),
                    ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.red)),
                        onPressed: deleteMovie,
                        child: Text("Delete Movie"))
                  ],
                )
        ],
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key}) : super(key: key);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _movies = Observable([
    Movie(
        id: 1,
        director: "Michael Bay",
        genres: [
          Genre(name: "Action", choosed: true),
          Genre(name: "Comedy"),
          Genre(name: "Fantasy"),
          Genre(name: "Horror"),
          Genre(name: "Sci-Fi", choosed: true)
        ],
        title: "Transformer"),
    Movie(
        id: 2,
        director: "Denis Villeneuve",
        genres: [
          Genre(name: "Action"),
          Genre(name: "Comedy"),
          Genre(name: "Fantasy", choosed: true),
          Genre(name: "Horror"),
          Genre(name: "Sci-Fi", choosed: true)
        ],
        title: "Arrival"),
    Movie(
        id: 3,
        director: "John Francis Daley",
        genres: [
          Genre(name: "Action"),
          Genre(name: "Comedy", choosed: true),
          Genre(name: "Fantasy"),
          Genre(name: "Horror", choosed: true),
          Genre(name: "Sci-Fi")
        ],
        title: "Game Night"),
  ]);

  void _goToAddorEditPage(Movie movie) {
    setState(() {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AddMovie(
            movie: movie,
          ),
        ),
      );
    });
  }

  void refreshUI() {
    setState(() {});
  }

  @override
  void initState() {
    eventBus.on<DeleteMovie>().listen((movie) {
      _movies.value.removeWhere((e) => e.id == movie.id);
    });
    eventBus.on<Movie>().listen((movie) {
      _movies.value.add(movie);
    });
    eventBus.on<Refresh>().listen((_) {
      refreshUI();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Movie App"),
      ),
      body: ListView.builder(
          itemCount: _movies.value.length,
          itemBuilder: (context, index) {
            return ListTile(
              onTap: () {
                _goToAddorEditPage(_movies.value[index]);
              },
              leading: Icon(Icons.movie),
              title: Text(_movies.value[index].title),
              subtitle: Text(_movies.value[index].director),
              trailing: IconButton(
                icon: Icon(
                  Icons.edit,
                ),
                onPressed: () {
                  _goToAddorEditPage(_movies.value[index]);
                },
              ),
            );
          }),
      floatingActionButton: FloatingActionButton.extended(
        label: Text("Add New"),
        onPressed: () {
          _goToAddorEditPage(Movie(id: _movies.value.last.id + 1, genres: [
            Genre(name: "Action"),
            Genre(name: "Comedy"),
            Genre(name: "Fantasy"),
            Genre(name: "Horror"),
            Genre(name: "Sci-Fi")
          ]));
        },
        tooltip: 'Increment',
        icon: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class Movie {
  int id;
  String title;
  String director;
  List<Genre> genres;
  String summary;

  Movie(
      {this.id = 0,
      this.director = "",
      this.summary = "",
      required this.genres,
      this.title = ""});
}

class Genre {
  String name;
  bool choosed;
  Genre({this.choosed = false, this.name = ""});
}

class Refresh {
  Refresh();
}

class DeleteMovie {
  int id;
  DeleteMovie({required this.id});
}
